import { createStore } from 'redux';
import rootReducer from './reducers';

const initialState = {};

let store = createStore(rootReducer, initialState);

export default store;
