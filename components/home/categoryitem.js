import React from "react";
import { useSelector } from "react-redux";

function CategoryItem(props) {
  let href = `#tab-${props.data.id}`;

  const languages = useSelector((state) => state.language);
  const filtered = languages.filter((item) => item.isDefault);
  const lang = filtered[0];
  const navTabsLiAActiveH3 = {
    color: "black",
    borderBottom: "black solid 2px",
  };

  return (
    <li role="presentation">
      <a
        href={href}
        aria-controls="home"
        role="tab"
        data-toggle="tab"
        className={props.index == 0 ? "active" : ""}
      >
        <img
          src={props.data.image}
          alt={props.data[lang.short_name]}
          className="category-item"
          loading="lazy"
        />
        <h3 >{props.data[lang.short_name]}</h3>
      </a>
    </li>
  );
}

export default CategoryItem;
