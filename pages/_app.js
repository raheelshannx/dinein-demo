import React, { Suspense } from "react";

import Link from "next/link";
import Head from "next/head";
import Script from "../components/home/script";
import { useState, useEffect } from "react";
import { Provider } from "react-redux";
import store from "../redux/index";
import Header from "../header";

export default function MyApp({ Component, pageProps }) {
  const Layout = Component.Layout ? Component.Layout : React.Fragment;

  return (
    <Provider store={store}>
      <Layout>
        <Header />

        <div className="content">
          <Component {...pageProps} />
        </div>
        <Script />
      </Layout>
    </Provider>
  );
}
